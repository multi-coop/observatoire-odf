> :warning:
> **Projet déplacé [vers l'instance gitlab d'odf](https://git.opendatafrance.net/observatoire/observatoire-odf)**


# Observatoire OpenDataFrance

Mise en place d'un fichier recenseant les organismes publics n'ayant jamais publié un jeu de données en open-data.
Cette liste a pour objectif de les contacter afin de les aider à ouvrir certaines de leurs données. 

## Mapping données Insee & ODF 

ODF publie un jeu de données des organismes publiant leur jeux de données.
Dans un premier temps nous avons réalisé un mapping pour vérifier le code juridique légale de l'entité (donnée INSEE) vis à vis du code rédigé par ODF.

En voici le résultat :

- 897 entités recencées par ODF ont déjà publié un jeu de données en open-data
- Parmis ces 897 entités, au moins 277 entités ne sont pas obligés de le faire car elle embauche moins de 50 agents.


| code Juridique Legale | Qu'en dit la base INSEE | Code ODF | Commentaire |
| --- | --- | --- | --- |  
| 7345 | Syndicat intercommunal à vocation multiple (SIVOM) | EPT & AGCT | |
| 7343 | Communauté urbaine | CU & CA | Incohérence |
| 5710 | SAS, société par actions simplifiée | DSPT | |
| 5599 | SA à conseil d'administration (s.a.i.) | DSPT | |  
| 7361 | Centre communal d'action sociale | AGCT | |
| 5515 | SA d'économie mixte à conseil d'administration | DSPT | |
| 5532 | SA d'intérêt collectif agricole (SICA) à conseil d'administration | AGCT | |
| 5202 | Société en nom collectif | DSPT | |
| 9260 | Association de droit local (Bas-Rhin, Haut-Rhin et Moselle) | OACT | |
| 7229 | (Autre) Collectivité territoriale | MET & COM | Incohérence |
| 4120 | Établissement public national à caractère industriel ou commercial non doté d'un comptable public  | DSPT | |
| 4110 | Établissement public national à caractère industriel ou commercial doté d'un comptable public | DSPT | |
| 7172 | Service déconcentré de l'État à compétence (inter) départementale  | SDE | |
| 7379 |(Autre) Établissement public administratif local | OACT | |
| 7220 | Département | DEP | |
| 7348 | Communauté d'agglomération  | CA __& CC__ | Incohérence |
| 7210 | Commune et commune nouvelle  | COM | |
| 4140 | Établissement public local à caractère industriel | OACT & DSPT | Incohérence |
| 7346 | Communauté de communes | CC | |
| 5499 | Société à responsabilité limitée (sans autre indication) | DSPT | |
| 7344 | Métropole | MET | |
| 4150 | Régie d'une collectivité locale à caractère industriel ou commercial  | DSPT | |
| 7355 | Syndicat mixte ouvert | AGCT & OACT |  ~~Incohérence~~ |
| 7354 | Syndicat mixte fermé | AGCT & OACT | ~~Incohérence~~ |
| 9220 | Association déclarée | OACT & AGCT | ~~Incohérence~~|
| 7230 | Région | REG | |
| 7372 | Service départemental d'incendie et de secours (SDIS) | OACT | |

       
